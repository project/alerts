(function (Drupal, drupalSettings, once) {
  'use strict';

  Drupal.behaviors.alertsDismiss = {
    attach: function(context, settings) {
      once('alertsAddDismiss', 'div.alert-banner', context).forEach(function(element) {
        augmentAlert(element.firstElementChild);
      });

      var dismissed = [];
      if (localStorage.getItem('dismissed')) {
        // If previously dismissed in local storage, hide them.
        dismissed = JSON.parse(localStorage.getItem('dismissed'));
      }
      once('alertsStorage', 'html', context).forEach(function () {
        // An alert being will be omitted from the list, so bypass updates.
        var viewingAlert = document.body.classList.contains('page-node-type-alert');
        var missing_found = false;
        if (dismissed.length) {
          // If previously dismissed in local storage, hide them.
          dismissed = JSON.parse(localStorage.getItem('dismissed'));
          dismissed.forEach(function(element, index) {
            alert = document.getElementById(element);
            if (alert) {
              alert.style.display = "none";
            }
            else if (!viewingAlert) {
              // Remove an alert that can't be found, if not viewing an alert.
              dismissed.splice(index, 1);
              missing_found = true;
            }
          });
        }
        if (missing_found) {
          // Save the updated list of dismissed banner ids.
          localStorage.setItem('dismissed', JSON.stringify(dismissed));
        }
      });

      // Add/change inputs based on initial config.
      function augmentAlert(element) {
        let btn = document.createElement("button");
        let label = '<span class="visually-hidden">';
        label += Drupal.t('Dismiss Alert');
        label += '</span>';
        btn.innerHTML = label;
        btn.classList.add("messages__close");
        btn.addEventListener("click", function (event) {
          buttonClick(element);
          event.preventDefault();
        }, false);
        element.appendChild(btn);
      }

      // Process a button click.
      function buttonClick(element) {
        let parent = element.parentNode;
        parent.style.display = "none";
        dismissed.push(parent.id);
        localStorage.setItem('dismissed', JSON.stringify(dismissed));
      }
    }
  };
} (Drupal, drupalSettings, once));
