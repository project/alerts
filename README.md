# Alerts

This module creates a system for storing and displaying alerts on your website.
Visitors can click a banner to view the details, or they can dismiss a banner,
and dismissed banners have their ids stored in the browser's localStorage, so
the same banner won't be shown on other pages or subsequent visits.

In addition to creating a content type for alerts, this also provides a
view for displaying them, sorted chronologically by creation with the most
recent first. It also creates a new taxonomy vocabulary, which can be used to
define the severity options available to content creators.

An optional submodule is provided to allow better formtting, and the JavaScript
to store which banners have been dismissed. This module is optimized for the
Olvero theme, but may be compatible with other themes too.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/alerts).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/alerts).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the Auto Entity Label module, but if you will want to
customize the way titles are generated (for example to include a title such as
"Dr." or degrees) then the inclusion of the Token module is also recommended.


## Installation

- Install the Alerts module as you would normally install a contributed Drupal
  module. Visit `https://www.drupal.org/node/1897420` for further information.
  We strongly recommend using composer to ensure all dependencies will be
  handled automatically.
- This will import configuration. Once installed, the module does dynamically
  generate CSS markup to properly set the background colors of banners when
  displayed at the top of the site. If you don't  require this (for example if
  hard-coding the background colors into your theme instead) the module can be
  uninstalled. Note that you won't be able to install it again on the same
  site, unless you delete the Alert bundle, Alert Severity vocabulary, and
  Alerts view that were installed originally.
- There is also an Alerts Olivero submodule that is recommended if using this
  module on a site using the Olivero theme. If using a custom theme, you may
  need to implement similar CSS to what is found in the submodule.


## Configuration

1. When installed, the provided Alerts view will have two displays: one for a
   standard list display, and the other a block showing up to 5 most recently
   posted alerts.
2. You may want to customize what information is captured and displayed for the
   Alert nodes on your site. If so, add fields according to the documentation
   at `https://www.drupal.org/docs/7/nodes-content-types-and-fields/add-a-field-to-a-content-type`
3. You may also want to add to the Alert Severity vocabulary, for example to add
   a SVG Image Field that would allow for a specific icon to appear on alert
   banners. Note that if implementing this you will need to add a mechanism to
   get the icon to display. One approach would be to update the Alerts view
   with a relationship to the taxonomy term, and then place the icon field into
   the display. Alternatively (and especially if using a custom theme) you could
   customize the code that dynamically generates CSS (provided inside
   alerts.module) to include whatever fields and formatting you need, inside
   your theme.


## Maintainers

- Martin Anderson-Clutz - [mandclu](https://www.drupal.org/u/mandclu)